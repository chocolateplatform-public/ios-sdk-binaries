//
//  ChocolatePlatform.h
//  test_App
//
//  Created by Lev Trubov on 3/10/18.
//  Copyright © 2018 Chocolate Platform. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChocolatePlatformInformation.h"

typedef NS_ENUM(NSUInteger, ChocolatePlatformFullscreenAdState) {
    CHPFullscreenAdStateLoading,
    CHPFullscreenAdStateReadyToLoad,
    CHPFullscreenAdStateFailedToLoad
};

typedef NS_ENUM(NSUInteger, ChocolatePlatformNoAdReason) {
    CHPNoAdReasonUnknown,
    CHPNoAdReasonNoFill,
    CHPNoAdReasonNetworkError,
    CHPNoAdReasonCappedOrPaced,
    CHPNoAdReasonPlaybackError,
    CHPNoAdReasonInternalError
};

@interface ChocolatePlatform : NSObject

+(ChocolatePlatformDemographics *)demographics;
+(ChocolatePlatformLocation*)location;
+(ChocolatePlatformAppInfo*)appInfo;
+(ChocolatePlatformPrivacySettings *)privacySettings;

+(void)saveAdUnitID:(NSString*)apiKey __attribute__((deprecated("Use initWithAdUnitID: at app launch")));
+(NSString *)getAdUnitID;
+(void)setCoppaStatus:(BOOL)enabled __attribute__((deprecated("Set COPPA status on chocolateplatform.com")));

+(void)initWithAdUnitID:(NSString *)apiKey;


@end
