//
//  ChocolatePlatform_SDK_Core.h
//  ChocolatePlatform-SDK-Core
//
//  Created by Lev Trubov on 1/16/18.
//  Copyright © 2018 Chocolate Platform. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ChocolatePlatform_SDK_Core.
FOUNDATION_EXPORT double ChocolatePlatform_SDK_CoreVersionNumber;

//! Project version string for ChocolatePlatform_SDK_Core.
FOUNDATION_EXPORT const unsigned char ChocolatePlatform_SDK_CoreVersionString[];

#import <ChocolatePlatform_SDK_Core/ChocolateAdConstants.h>
#import <ChocolatePlatform_SDK_Core/ChocolateAd.h>

#import <ChocolatePlatform_SDK_Core/ChocolatePlatform.h>
#import <ChocolatePlatform_SDK_Core/ChocolatePlatformInformation.h>
#import <ChocolatePlatform_SDK_Core/ChocolatePlatformCustomSegmentProperties.h>



