//
//  ChocolateAd.h
//  test_App
//
//  Created by Lev Trubov on 10/17/19.
//  Copyright © 2019 Chocolate Platform. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChocolateAdConstants.h"
#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChocolateReward : NSObject

+(instancetype) blankReward;

@property NSUInteger rewardAmount;
@property (copy) NSString *rewardName;
@property (copy) NSString *userID;
@property (copy) NSString *secretKey;

@end

@class ChocolateAd;

@protocol ChocolateAdDelegate

-(void)onChocolateAdLoaded:(ChocolateAd *)ad;
-(void)onChocolateAdLoadFailed:(ChocolateAd *)ad because:(ChocolateAdNoAdReason)reason;
-(void)onChocolateAdShown:(ChocolateAd *)ad;
-(void)onChocolateAdClosed:(ChocolateAd *)ad;

@optional
-(void)onChocolateAdClicked:(ChocolateAd *)ad;
-(void)onChocolateAdFailedToStart:(ChocolateAd *)ad because:(ChocolateAdNoAdReason)reason;
-(void)onChocolateAdReward:(NSString *)rewardName amount:(NSInteger)rewardAmount;

@end

@interface ChocolateAd : NSObject

-(instancetype)initWithDelegate:(id<ChocolateAdDelegate>)delegate;
-(void)load;
-(void)loadPlacement:(NSString *)placement;
-(NSString *)winningPartnerName;
-(ChocolateAdLoadingState)adState;
-(ChocolateAdLoadingState)adStateForPlacement:(nullable NSString *)placement;
-(void)selectPartners:(NSArray *)partnerList;
-(void)addCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeCustomTargeting:(NSString *)key as:(NSString *)target;
-(void)removeAllCustomTargeting;

@property(weak) id<ChocolateAdDelegate> delegate;

@end

@interface ChocolateFullscreenAd : ChocolateAd
-(void)showFrom:(UIViewController *)viewController;
@end

@interface ChocolateInterstitialAd : ChocolateFullscreenAd
@end

@interface ChocolateRewardedAd : ChocolateFullscreenAd
@property ChocolateReward *reward;
@end

@interface ChocolateEmbeddedAd : ChocolateAd
-(void)showIn:(UIView *)view at:(CGPoint)position;
@end

@interface ChocolateBannerAd : ChocolateEmbeddedAd
@property ChocolateBannerAdSize size;
-(void)close;
@property(readonly) NSTimeInterval refreshInterval;
@end

@interface ChocolatePrerollAd : ChocolateEmbeddedAd
-(void)showOnPlayer:(AVPlayerViewController *)videoToInterrupt;
@property ChocolatePrerollAdSize size;

@end

NS_ASSUME_NONNULL_END
