require 'fileutils'

SOURCE = [
  'Mediation',
  'SDK',
  'Plugins',
  'LICENCE.md'
]
EXCLUDE = [
  'Mediation/AdColony/README.md',
  'Mediation/vungle/README.md',
  'Mediation/Google/README.txt'
]
REARRANGE = {}

DEST = 'ChocolatePlatform_iOS_SDK'

FileUtils.rm_r(DEST) if Dir.exist?(DEST)
FileUtils.mkdir(DEST)

SOURCE.each { |sf| FileUtils.cp_r(sf,DEST) }
REARRANGE.each { |s,d| FileUtils.cp_r("#{DEST}/#{s}", "#{DEST}/#{d}") }
EXCLUDE.each do |ef|
  fn = "#{DEST}/#{ef}"
  FileUtils.rm_r(fn) if File.exist?(fn) # file not being there is not an error
end
