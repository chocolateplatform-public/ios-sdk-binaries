//
//  VdopiaiOSBridge.m
//  SimpleGame
//
//  Created by Ashutosh Kulkarni on 8/25/17.
//
//

#import "ChocolatePlatformCC2DXBridge.h"
#import "ChocolatePlatformCPPAdRequester.h"




NSString * nsstringify(const char *source){
    return [NSString stringWithCString:source encoding:NSUTF8StringEncoding];
}

#pragma mark - init

void initWithAPIKey(const char *adUnitID) {
    [[ChocolatePlatformCPPAdRequester sharedObject] initWithAPIKey:nsstringify(adUnitID)];
}

#pragma mark - reward ad


void loadRewardAd(const char *adUnitID)
{
    [[ChocolatePlatformCPPAdRequester sharedObject] loadRewardAd:nsstringify(adUnitID)];
}

void showRewardAd(int rewardAmount,const char* rewardName, const char* usedId, const char* secretKey)
{
    [[ChocolatePlatformCPPAdRequester sharedObject]
     showRewardAd:rewardAmount
     rewardName:nsstringify(rewardName)
     userID:nsstringify(usedId)
     secretKey:nsstringify(secretKey)];
}

#pragma mark - InterstitialAd

void loadInterstitialAd(const char *adUnitID)
{
    [[ChocolatePlatformCPPAdRequester sharedObject] loadInterstitialAd:nsstringify(adUnitID)];
}

void showInterstitialAd()
{
   [[ChocolatePlatformCPPAdRequester sharedObject] showInterstitialAd];
}

#pragma mark - parameter settings

void SetAdRequestUserParams(const char* age,
                       const char* birthDate,
                       const char* gender,
                       const char* maritalStatus,
                       const char* ethnicity,
                       const char* dmaCode,
                       const char* postal,
                       const char* curPostal,
                       const char* latitude,
                       const char* longitude)
{
    [[ChocolatePlatformCPPAdRequester sharedObject]
     SetAdRequestUserParams:nsstringify(age)
     birthday:nsstringify(birthDate)
     gender:nsstringify(gender)
     maritalStatus:nsstringify(maritalStatus)
     ethnicity:nsstringify(ethnicity)
     dmaCode:nsstringify(dmaCode)
     postal:nsstringify(postal)
     curPostal:nsstringify(curPostal)
     latitude:nsstringify(latitude)
     longitude:nsstringify(longitude)];
}

void SetAdRequestAppParams(const char* appName,
                      const char* pubName,
                      const char* appDomain,
                      const char* pubDomain,
                      const char* storeUrl,
                      const char* iabCategory)
{
    [[ChocolatePlatformCPPAdRequester sharedObject]
     SetAdRequestAppParams:nsstringify(appName)
     pubName:nsstringify(pubName)
     appDomain:nsstringify(appDomain)
     pubDomain:nsstringify(pubDomain)
     storeUrl:nsstringify(storeUrl)
     iabCategory:nsstringify(iabCategory)];
}

void setPrivacySettings(bool gdprApplies, const char* gdprConsentString) {
    NSString *cs = nil;
    if(gdprConsentString){
        cs = nsstringify(gdprConsentString);
    }
    [[ChocolatePlatformCPPAdRequester sharedObject] setPrivacySettings:gdprApplies withConsent:cs];
}


static ChocolatePlatformInterstitialCallbackReceiver *currIntRec = nil;
static ChocolatePlatformRewardCallbackReceiver *currRewRec = nil;

void subscribeToInterstitialCallbacks(ChocolatePlatformInterstitialCallbackReceiver* receiver){
    currIntRec = receiver;
}

void subscribeToRewardCallbacks(ChocolatePlatformRewardCallbackReceiver* receiver){
    currRewRec = receiver;
}

ChocolatePlatformInterstitialCallbackReceiver* currentInterstitialCallbackReceiver(){
    return currIntRec;
}

ChocolatePlatformRewardCallbackReceiver* currentRewardCallbackReceiver(){
    return currRewRec;
}




