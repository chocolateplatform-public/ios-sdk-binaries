//
//  ChocolatePlatformCC2DXBridge.h
//  SimpleGame
//
//  Created by Ashutosh Kulkarni on 8/25/17.
//
//

#ifndef ChocolatePlatformCC2DXBridge_h
#define ChocolatePlatformCC2DXBridge_h

class ChocolatePlatformInterstitialCallbackReceiver {
public:
    virtual void interstitialAdLoaded() = 0;
    virtual void interstitialAdFailed() = 0;
    virtual void interstitialAdShown() = 0;
    virtual void interstitialAdClicked() = 0;
    virtual void interstitialAdDismissed() = 0;
};

class ChocolatePlatformRewardCallbackReceiver {
public:
    virtual void rewardedAdLoaded() = 0;
    virtual void rewardedAdFailed() = 0;
    virtual void rewardedAdShown() = 0;
    virtual void rewardedAdDismissed() = 0;
    virtual void rewardedAdFinished(int rewardAmount, const char *rewardName) = 0;
};

void initWithAPIKey(const char *adUnitID);

void loadInterstitialAd(const char *adUnitID);
void showInterstitialAd();
void loadRewardAd(const char *adUnitID);
void showRewardAd(int rewardAmount,const char* rewardName, const char* usedId, const char* secretKey);
void SetAdRequestUserParams(const char* age, const char* birthDate, const char* gender, const char* maritalStatus,
                            const char* ethnicity, const char* dmaCode, const char* postal, const char* curPostal,const char* latitude,const char* longitude);

void SetAdRequestAppParams(const char* appName, const char* pubName,
                           const char* appDomain, const char* pubDomain,
                           const char* storeUrl, const char* iabCategory);

void setPrivacySettings(bool gdprApplies, const char* gdprConsentString);

void subscribeToInterstitialCallbacks(ChocolatePlatformInterstitialCallbackReceiver* receiver);
void subscribeToRewardCallbacks(ChocolatePlatformRewardCallbackReceiver* receiver);

ChocolatePlatformInterstitialCallbackReceiver* currentInterstitialCallbackReceiver();
ChocolatePlatformRewardCallbackReceiver* currentRewardCallbackReceiver();




#endif /* ChocolatePlatformCC2DXBridge_h */
