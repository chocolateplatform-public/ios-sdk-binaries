//
//  ChocolatePlatformCPPAdRequester.h
//  VdopiaCoco2dxDemo-mobile
//
//  Created by Ashutosh Kulkarni on 12/25/17.
//

#import <Foundation/Foundation.h>
#import "ChocolatePlatformAdRequester.h"

@interface ChocolatePlatformCPPAdRequester : ChocolatePlatformAdRequester
+ (instancetype)sharedObject;

@end
