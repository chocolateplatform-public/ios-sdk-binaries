    //
    //  ChocolatePlatformCPPAdRequester.m
    //  VdopiaCoco2dxDemo-mobile
    //
    //  Created by Ashutosh Kulkarni on 12/25/17.
    //

#import <ChocolatePlatform_SDK_Core/ChocolatePlatform_SDK_Core.h>
#import "ChocolatePlatformCPPAdRequester.h"
#import "ChocolatePlatformCC2DXBridge.h"


@implementation ChocolatePlatformCPPAdRequester

+ (instancetype)sharedObject {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

#pragma mark --------InterstitialAd--------

- (void)onInterstitialLoaded:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    currentInterstitialCallbackReceiver()->interstitialAdLoaded();
}

- (void)onInterstitialFailed:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd errorCode:(int)errorCode {
    currentInterstitialCallbackReceiver()->interstitialAdFailed();
}

- (void)onInterstitialShown:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    currentInterstitialCallbackReceiver()->interstitialAdShown();
}

- (void)onInterstitialClicked:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    currentInterstitialCallbackReceiver()->interstitialAdClicked();
}

- (void)onInterstitialDismissed:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    
    currentInterstitialCallbackReceiver()->interstitialAdDismissed();
    [super onInterstitialDismissed:interstitialAd];

}

#pragma mark reward ad delegate
- (void)rewardedVideoDidLoadAd:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    currentRewardCallbackReceiver()->rewardedAdLoaded();
}

- (void)rewardedVideoDidStartVideo:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    currentRewardCallbackReceiver()->rewardedAdShown();
}

- (void)rewardedVideoWillDismiss:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    currentRewardCallbackReceiver()->rewardedAdDismissed();
}

- (void)rewardedVideoDidFailToLoadAdWithError:(int)error rewardAdView:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    currentRewardCallbackReceiver()->rewardedAdFailed();
}

- (void)rewardedVideoDidFailToStartVideoWithError:(int)error rewardAdView:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    currentRewardCallbackReceiver()->rewardedAdFailed();
}

- (void)rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(NSString *)rewardName
{
    currentRewardCallbackReceiver()->rewardedAdFinished((int)rewardAmount, [rewardName cStringUsingEncoding:NSUTF8StringEncoding]);
}

@end
