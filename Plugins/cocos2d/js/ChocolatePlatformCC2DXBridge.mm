//
//  ChocolatePlatformCC2DXBridge.m
//  SimpleGame
//
//  Created by Ashutosh Kulkarni on 8/25/17.
//
//

#import "ChocolatePlatformCC2DXBridge.h"
#import "ChocolatePlatformJSAdRequester.h"

#include "cocos/scripting/js-bindings/jswrapper/SeApi.h"

@implementation ChocolatePlatformCC2DXBridge

#pragma mark - init

+ (void)initWithAPIKey:(NSString *)adUnitID {
    [[ChocolatePlatformJSAdRequester sharedObject] initWithAPIKey:adUnitID];
}

#pragma mark - reward ad

+(BOOL) loadRewardAd:(NSString*) adUnitID
{
    [[ChocolatePlatformJSAdRequester sharedObject]loadRewardAd:adUnitID];
    return true;
}

+ (void) showRewardAd:(NSString*)rewardAmount
          rewardName:(NSString*)rewardName
              usedId:(NSString*)usedId
           secretKey:(NSString*)secretKey
{
    [[ChocolatePlatformJSAdRequester sharedObject] showRewardAd:[rewardAmount intValue] rewardName:rewardName userID:usedId secretKey:secretKey];
}

+ (void)sendAdCallbacks:(NSString*)adState adType:(NSString*)adType
{
    NSString* string  = [NSString stringWithFormat:@"var vdopia = require('ChocolatePlatformCocosPlugin'); vdopia.prototype.vdopiaAdEvent('%@','%@')",adType,adState];
    se::ScriptEngine* se = se::ScriptEngine::getInstance();
    se->evalString([string UTF8String]);
}

+ (void)sendRewardAdCallbacks:(NSString*)adState
{
    
}

#pragma mark - InterstitialAd

+ (void) loadInterstitialAd:(NSString*)adUnitID
{
    [[ChocolatePlatformJSAdRequester sharedObject] loadInterstitialAd:adUnitID];
}

+(void)showInterstitialAd
{
   [[ChocolatePlatformJSAdRequester sharedObject] showInterstitialAd];
}

#pragma mark - parameter settings

+ (void) SetAdRequestUserParams:(NSString*)age
birthDate:(NSString*)birthDate
gender:(NSString*) gender
maritalStatus:(NSString*) maritalStatus
ethnicity:(NSString*)ethnicity
dmaCode:(NSString*)dmaCode
postal:(NSString*)postal
curPostal:(NSString*)curPostal
latitude:(NSString*)latitude
longitude:(NSString*)longitude
{
    [[ChocolatePlatformJSAdRequester sharedObject]SetAdRequestUserParams:age birthday:birthDate gender:gender maritalStatus:maritalStatus ethnicity:ethnicity dmaCode:dmaCode postal:postal curPostal:curPostal latitude:latitude longitude:longitude];
}

+ (void) SetAdRequestAppParams:(NSString*)appName
                      pubName:(NSString*)pubName
                    appDomain:(NSString*) appDomain
                    pubDomain:(NSString*) pubDomain
                     storeUrl:(NSString*) storeUrl
                  iabCategory:(NSString*) iabCategory
{
    [[ChocolatePlatformJSAdRequester sharedObject]SetAdRequestAppParams:appName pubName:pubName appDomain:appDomain pubDomain:pubDomain storeUrl:storeUrl iabCategory:iabCategory];
}

+(void)setPrivacySettings:(BOOL)gdprApplies withConsent:(NSString *)gdprConsentString {
    [[ChocolatePlatformJSAdRequester sharedObject] setPrivacySettings:gdprApplies withConsent:gdprConsentString];
}



+ (void)sendMessageToCocos:(NSString*)string
{
    
    
}

@end
