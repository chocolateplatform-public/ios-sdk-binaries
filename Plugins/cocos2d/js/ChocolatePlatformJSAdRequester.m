    //
    //  ChocolatePlatformJSAdRequester.m
    //  VdopiaCoco2dxDemo-mobile
    //
    //  Created by Ashutosh Kulkarni on 12/25/17.
    //

//#import <ChocolatePlatform_SDK_Core/ChocolatePlatform_SDK_Core.h>
#import "ChocolatePlatformJSAdRequester.h"
#import "ChocolatePlatformCC2DXBridge.h"
//#import <CoreLocation/CoreLocation.h>

#define INTERSTITIAL_AD_LOADED @"INTERSTITIAL_LOADED"
#define  INTERSTITIAL_AD_FAILED  @"INTERSTITIAL_FAILED"
#define  INTERSTITIAL_AD_SHOWN  @"INTERSTITIAL_SHOWN"
#define  INTERSTITIAL_AD_DISMISSED  @"INTERSTITIAL_DISMISSED"
#define  INTERSTITIAL_AD_CLICKED  @"INTERSTITIAL_CLICKED"

#define REWARD_AD_LOADED  @"REWARD_AD_LOADED"
#define REWARD_AD_FAILED @"REWARD_AD_FAILED"
#define REWARD_AD_DISMISSED  @"REWARD_AD_DISMISSED"
#define REWARD_AD_STARTED  @"REWARD_AD_STARTED"
#define REWARD_AD_FAILED_TO_START  @"REWARD_AD_FAILED_TO_START"
#define REWARD_AD_COMPLETED  @"REWARD_AD_COMPLETED"

#define INTERSTITIAL_AD_TYPE @"INTERSTITIAL"
#define REWARD_AD_TYPE @"REWARD"

@interface ChocolatePlatformJSAdRequester() <ChocolatePlatformInterstitialAdDelegate,ChocolatePlatformRewardAdDelegate>
@property (nonatomic,strong) ChocolatePlatformInterstitialAdDisplay* interstitial;
@property (nonatomic,strong) ChocolatePlatformRewardAdDisplay* reward;
@end

@implementation ChocolatePlatformJSAdRequester

+ (instancetype)sharedObject {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

# pragma mark - Interstitial ad delegate Methods
- (void)onInterstitialLoaded:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:INTERSTITIAL_AD_LOADED adType:INTERSTITIAL_AD_TYPE];
}

- (void)onInterstitialFailed:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd errorCode:(int)errorCode {
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:INTERSTITIAL_AD_FAILED adType:INTERSTITIAL_AD_TYPE];
}

- (void)onInterstitialShown:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:INTERSTITIAL_AD_SHOWN adType:INTERSTITIAL_AD_TYPE];
}

- (void)onInterstitialClicked:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:INTERSTITIAL_AD_CLICKED adType:INTERSTITIAL_AD_TYPE];
}

- (void)onInterstitialDismissed:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:INTERSTITIAL_AD_DISMISSED adType:INTERSTITIAL_AD_TYPE];
    [super onInterstitialDismissed:interstitialAd];
}

- (void)sendIntrestialAdCallbacks:(NSString*)adState
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:adState adType:INTERSTITIAL_AD_TYPE];
}

#pragma mark reward ad delegate
- (void)rewardedVideoDidLoadAd:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:REWARD_AD_LOADED adType:REWARD_AD_TYPE];
}

- (void)rewardedVideoDidStartVideo:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:REWARD_AD_STARTED adType:REWARD_AD_TYPE];
}

- (void)rewardedVideoWillDismiss:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:REWARD_AD_DISMISSED adType:REWARD_AD_TYPE];
}

- (void)rewardedVideoDidFailToLoadAdWithError:(int)error rewardAdView:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:REWARD_AD_FAILED adType:REWARD_AD_TYPE];
}

- (void)rewardedVideoDidFailToStartVideoWithError:(int)error rewardAdView:(ChocolatePlatformRewardAdDisplay *)rewardedAd
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:REWARD_AD_FAILED_TO_START adType:REWARD_AD_TYPE];
}

- (void)rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(NSString *)rewardName
{
    [ChocolatePlatformCC2DXBridge sendAdCallbacks:REWARD_AD_COMPLETED adType:REWARD_AD_TYPE];
}
@end
