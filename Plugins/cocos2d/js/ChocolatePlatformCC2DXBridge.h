//
//  ChocolatePlatformCC2DXBridge.h
//  SimpleGame
//
//  Created by Ashutosh Kulkarni on 8/25/17.
//
//

#import <UIKit/UIKit.h>
#import <ChocolatePlatform_SDK_Core/ChocolatePlatform_SDK_Core.h>

@interface ChocolatePlatformCC2DXBridge : UIViewController

#pragma mark - init

+ (void)initWithAPIKey:(NSString *)adUnitID;

#pragma mark - rewardAd
+ (BOOL) loadRewardAd:(NSString*) adUnitID;
//+ (BOOL) isRewardAdAvailableToShow;
+(void) showRewardAd:(NSString*)rewardAmount
          rewardName:(NSString*)rewardName
              usedId:(NSString*)usedId
           secretKey:(NSString*)secretKey;

#pragma mark - InterstitialAd
+ (void) loadInterstitialAd:(NSString*)adUnitID;
+ (void) showInterstitialAd;

#pragma mark - parameters
+ (void) SetAdRequestUserParams:(NSString*)age birthDate:(NSString*)birthDate gender:(NSString*) gender maritalStatus:(NSString*) maritalStatus ethnicity:(NSString*)ethnicity dmaCode:(NSString*)dmaCode postal:(NSString*)postal curPostal:(NSString*)curPostal latitude:(NSString*)latitude longitude:(NSString*)longitude;
+ (void) SetAdRequestAppParams:(NSString*)appName pubName:(NSString*)pubName appDomain:(NSString*) appDomain pubDomain:(NSString*) pubDomain storeUrl:(NSString*) storeUrl iabCategory:(NSString*) iabCategory;
//+ (void) setFacebookHashID:(NSString*) facebookHashID;

+ (void)sendAdCallbacks:(NSString*)adState adType:(NSString*)adType;
+ (void)setPrivacySettings:(BOOL)gdprApplies withConsent:(NSString *)gdprConsentString;

@end
