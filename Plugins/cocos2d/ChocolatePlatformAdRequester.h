//
//  ChocolatePlatformAdRequester.h
//  VdopiaCoco2dxDemo-mobile
//
//  Created by Ashutosh Kulkarni on 12/25/17.
//

#import <Foundation/Foundation.h>
#import <ChocolatePlatform_SDK_Core/ChocolatePlatform_SDK_Core.h>

@interface ChocolatePlatformAdRequester : NSObject <ChocolatePlatformInterstitialAdDelegate,ChocolatePlatformRewardAdDelegate>

#pragma mark init

- (void)initWithAPIKey:(NSString *)adUnitID;

#pragma mark InterstitialAd
- (void) loadInterstitialAd:(NSString*)adUnitID;
- (void) showInterstitialAd;

#pragma mark RewardAd
- (void) loadRewardAd:(NSString*)adUnitID;
- (void) showRewardAd:(int)rewardAmount rewardName:(NSString*)rewardName userID:(NSString*)usedId secretKey:(NSString*) secretKey;

#pragma mark parameters
- (void) SetAdRequestUserParams:(NSString*)age
                       birthday:(NSString*)birthDate
                         gender:(NSString*)gender
                  maritalStatus:(NSString*)maritalStatus
                      ethnicity:(NSString*)ethnicity
                        dmaCode:(NSString*)dmaCode
                         postal:(NSString*) postal
                      curPostal:(NSString*) curPostal
                       latitude:(NSString*)latitude
                      longitude:(NSString*)longitude;
- (void) SetAdRequestAppParams:(NSString*)appName
                       pubName:(NSString*) pubName
                     appDomain:(NSString*) appDomain
                     pubDomain:(NSString*) pubDomain
                      storeUrl:(NSString*)storeUrl
                   iabCategory:(NSString*) iabCategory;

-(void)setPrivacySettings:(BOOL)gdprApplies withConsent:(NSString *)gdprConsentString;
@end
