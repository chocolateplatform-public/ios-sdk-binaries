    //
    //  ChocolatePlatformAdRequester.m
    //  VdopiaCoco2dxDemo-mobile
    //
    //  Created by Ashutosh Kulkarni on 12/25/17.
    //

#import "ChocolatePlatformAdRequester.h"
#import <CoreLocation/CoreLocation.h>

@interface ChocolatePlatformAdRequester() 
@property (nonatomic,strong) ChocolatePlatformInterstitialAdDisplay* interstitial;
@property (nonatomic,strong) ChocolatePlatformRewardAdDisplay* reward;

@end

@implementation ChocolatePlatformAdRequester

#pragma mark init

- (void)initWithAPIKey:(NSString *)adUnitID {
    [ChocolatePlatform initWithAdUnitID:adUnitID];
}

#pragma mark --------InterstitialAd--------

- (void)loadInterstitialAd:(NSString*)adUnitID
{
    [self cleanInterstitialAd];
    [ChocolatePlatform saveAdUnitID:adUnitID];
    UIViewController* controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController* viewController =  [self visibleViewController:controller ];
    self.interstitial = [[ChocolatePlatformInterstitialAdDisplay alloc]
                         initWithAdUnitID:[ChocolatePlatform getAdUnitID]
                         delegate:self
            viewControllerForPresentingModalView:viewController];
    
    [self.interstitial load];
}

- (void) showInterstitialAd
{
    if (self.interstitial)
        {
            [self.interstitial show];
        }
}

- (void)cleanInterstitialAd
{
    [self.interstitial removeFromParentViewController];
    [self.interstitial.view removeFromSuperview];
    self.interstitial = nil;
}

# pragma mark - VDOInterstitialDelegate Methods
- (void)onInterstitialLoaded:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{}

- (void)onInterstitialFailed:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd errorCode:(int)errorCode {}

- (void)onInterstitialShown:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{}

- (void)onInterstitialClicked:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{}

- (void)onInterstitialDismissed:(ChocolatePlatformInterstitialAdDisplay*)interstitialAd{
    [self cleanInterstitialAd];
    interstitialAd = nil;
}

#pragma mark  ---------rewardAd-------------

- (void) loadRewardAd:(NSString*)adUnitID
{
    [ChocolatePlatform saveAdUnitID:adUnitID];
    self.reward = [[ChocolatePlatformRewardAdDisplay alloc]
                         initWithAdUnitID:[ChocolatePlatform getAdUnitID]
                         delegate:self];
    
    [self.reward load];
}


- (void) showRewardAd:(int)rewardAmount rewardName:(NSString*)rewardName userID:(NSString*)usedId secretKey:(NSString*) secretKey
{
    ChocolatePlatformRewardAdSettings *settings = [ChocolatePlatformRewardAdSettings blankSettings];
    settings.rewardName = rewardName;
    settings.rewardAmount = rewardAmount;
    settings.userID = usedId;
    settings.secretKey = secretKey;
    
    [self.reward show:settings];
}

#pragma mark reward ad delegate
- (void)rewardedVideoDidLoadAd:(ChocolatePlatformRewardAdDisplay *)rewardedAd{}

- (void)rewardedVideoDidStartVideo:(ChocolatePlatformRewardAdDisplay *)rewardedAd{}

- (void)rewardedVideoWillDismiss:(ChocolatePlatformRewardAdDisplay *)rewardedAd{}

- (void)rewardedVideoDidFailToLoadAdWithError:(int)error rewardAdView:(ChocolatePlatformRewardAdDisplay *)rewardedAd{}

- (void)rewardedVideoDidFailToStartVideoWithError:(int)error rewardAdView:(ChocolatePlatformRewardAdDisplay *)rewardedAd{}

- (void)rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(NSString *)rewardName{}

- (UIViewController*)rewardAdViewControllerForPresentingModalView
{
    UIViewController* controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    return [self visibleViewController:controller];
}

#pragma mark  ---------rewardAd End-------------

- (UIViewController*) visibleViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil)
        {
        return rootViewController;
        }
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]])
        {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        return  [self visibleViewController:lastViewController];
        }
    if ([rootViewController.presentedViewController isKindOfClass:[UITabBarController class]])
        {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController.presentedViewController;
        UIViewController *selectedViewController = tabBarController.selectedViewController;
        
        return [self visibleViewController:selectedViewController ];
        }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    
    return [self visibleViewController:presentedViewController];
}

- (void) SetAdRequestUserParams:(NSString*)age
                       birthday:(NSString*)birthDate
                         gender:(NSString*)gender
                  maritalStatus:(NSString*)maritalStatus
                      ethnicity:(NSString*)ethnicity
                        dmaCode:(NSString*)dmaCode
                         postal:(NSString*) postal
                      curPostal:(NSString*) curPostal
                       latitude:(NSString*)latitude
                      longitude:(NSString*)longitude
{
    ChocolatePlatformDemographics *dem = [ChocolatePlatform demographics];
    
    if ([self isStringValid:age]){
        dem.age = age.integerValue;
    }
    if ([self isStringValid:birthDate]){
        NSArray<NSString*> *comps = [birthDate componentsSeparatedByString:@"-"];
        if(comps.count >= 3){
            NSInteger year = [comps[0] integerValue];
            NSInteger month = [comps[1] integerValue];
            NSInteger day = [comps[2] integerValue];
            if(year != 0 && month != 0 && day != 0){
                [dem setBirthdayYear:year month:month day:day];
            }
        }
    }
    if ([self isStringValid:maritalStatus]){
        if([maritalStatus rangeOfString:@"single" options:NSCaseInsensitiveSearch].location != NSNotFound){
            dem.maritalStatus = ChocolatePlatformMaritalStatusSingle;
        }else if([maritalStatus rangeOfString:@"married" options:NSCaseInsensitiveSearch].location != NSNotFound){
            dem.maritalStatus = ChocolatePlatformMaritalStatusMarried;
        }else if([maritalStatus rangeOfString:@"divorced" options:NSCaseInsensitiveSearch].location != NSNotFound){
            dem.maritalStatus = ChocolatePlatformMaritalStatusDivorced;
        }else if([maritalStatus rangeOfString:@"widowed" options:NSCaseInsensitiveSearch].location != NSNotFound){
            dem.maritalStatus = ChocolatePlatformMaritalStatusWidowed;
        }else if([maritalStatus rangeOfString:@"separated" options:NSCaseInsensitiveSearch].location != NSNotFound){
            dem.maritalStatus = ChocolatePlatformMaritalStatusSeparated;
        }else if([maritalStatus rangeOfString:@"other" options:NSCaseInsensitiveSearch].location != NSNotFound){
            dem.maritalStatus = ChocolatePlatformMaritalStatusOther;
        }
    }
    if ([self isStringValid:gender]){
        NSString *genderString = gender;
        
        NSRange maleRange = [genderString rangeOfString:@"Male" options:NSCaseInsensitiveSearch];
        NSRange feMaleRange = [genderString rangeOfString:@"Female" options:NSCaseInsensitiveSearch];
        
        if (maleRange.location != NSNotFound){
            NSLog(@"Male");
            dem.gender = ChocolatePlatformGenderMale;
        }else if (feMaleRange.location != NSNotFound){
            NSLog(@"Female");
            dem.gender = ChocolatePlatformGenderFemale;
        }else if (feMaleRange.location != NSNotFound){
            NSLog(@"other");
            dem.gender = ChocolatePlatformGenderOther;
        }
    }
    
    if ([self isStringValid:ethnicity]){
        dem.ethnicity = ethnicity;
    }
    
    ChocolatePlatformLocation *loc = [ChocolatePlatform location];
    
    if ([self isStringValid:dmaCode]){
        loc.dmacode = dmaCode;
    }
    if ([self isStringValid:postal]){
        loc.postalcode = postal;
    }
    if ([self isStringValid:curPostal]){
        loc.currpostal = curPostal;
    }
    
    if ([self isStringValid:latitude] && [self isStringValid:longitude]){
        
        CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        
        
        loc.location = LocationAtual;
        
    }
}

-(void)setPrivacySettings:(BOOL)gdprApplies withConsent:(NSString *)gdprConsentString {
    ChocolatePlatformPrivacySettings *ps = [ChocolatePlatform privacySettings];
    [ps subjectToGDPR:gdprApplies withConsent:gdprConsentString];
}


- (void) SetAdRequestAppParams:(NSString*)appName
                       pubName:(NSString*) pubName
                     appDomain:(NSString*) appDomain
                     pubDomain:(NSString*) pubDomain
                      storeUrl:(NSString*)storeUrl
                   iabCategory:(NSString*) iabCategory
{
    ChocolatePlatformAppInfo *ai = [ChocolatePlatform appInfo];
    
    if ([self isStringValid:appName]){
        ai.appName = appName;
    }
    if ([self isStringValid:pubName ]) {
        ai.requester = pubName;
    }
    
    if ([self isStringValid:appDomain]) {
        ai.appDomain = appDomain;
    }
    
    if ([self isStringValid:pubDomain ]) {
        ai.publisherdomain = pubDomain;
    }
    
    if ([self isStringValid:storeUrl]) {
        ai.appStoreUrl = storeUrl;
    }
    
    if ([self isStringValid:iabCategory]){
        ai.Category = iabCategory;
    }
}

- (BOOL) isStringValid:(NSString*)string
{
    if ([string isKindOfClass:[NSNull class]])
        {
        return NO;
        }
    else if (string.length == 0)
        {
        return NO;
        }
    return YES;
}
@end
