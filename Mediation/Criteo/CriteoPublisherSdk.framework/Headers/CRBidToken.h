//
//  CRBidToken.h
//  CriteoPublisherSdk
//
//  Copyright © 2019 Criteo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CRBidToken : NSObject <NSCopying>

- (instancetype) init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
